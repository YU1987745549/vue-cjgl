import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component:()=>import ('@/views/login.vue')
    },
	{
	 path:'/table',
   name:'table',
   component:()=>import ('@/views/table')
	},

  {
   path:'/main',
   name:'main',
   component:()=>import ('@/views/main'),
   children:[
     {
      path:'/treeTable',
      name:'treeTable',
      component:()=>import ('@/views/treeTable')
     },
     {
       path:'/course',
       name:'course',
       component:()=>import ('@/views/course')
     },
     {
       path:'/teacher',
       name:'teacher',
       component:()=>import ('@/views/teacher')
     },
     {
       path:'/tbClass',
       name:'tbClass',
       component:()=>import ('@/views/tbClass')
     },
     {
       path:'/student',
       name:'student',
       component:()=>import ('@/views/student')
     },
     {
       path:'/setTbClassGrade',
       name:'setTbClassGrade',
       component:()=>import ('@/views/setTbClassGrade')
     },
     {
       path:'/setTbClassAndStudent',
       name:'setTbClassAndStudent',
       component:()=>import ('@/views/setTbClassAndStudent')
     },
     {
       path:'/findIpGrade',
       name:'findIpGrade',
       component:()=>import ('@/views/findIpGrade')
     },
     {
       path:'/findtbClass',
       name:'findtbClass',
       component:()=>import ('@/views/findtbClass')
     },
     {
       path:'/stuTbClassGrade',
       name:'stuTbClassGrade',
       component:()=>import ('@/views/stuTbClassGrade')
     },
     {
       path:'/stuIpGrade',
       name:'stuIpGrade',
       component:()=>import ('@/views/stuIpGrade')
     }
   ]
  }
  ]
})
