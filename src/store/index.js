import Vue from 'vue'
import Vuex from 'vuex'
import router from '../router/index.js'

Vue.use(Vuex)

const store = new Vuex.Store({
  //全局变量
  state: {
    name:undefined,
    user: undefined,
    isLogined: undefined,
    option: undefined,
    idxed: -1,
    num: -1,
    dialogClose:undefined,
    formData:undefined
  },
  // 修改全局变量必须通过mutations中的方法
  // mutations只能采用同步方法
  mutations: {
    setFormData(state,formData){
      state.formData=formData
    },
    setDialogClose(state,dialogClose){
      state.dialogClose=dialogClose;
    },
    setUser(state, user) {
      state.user = user;
    },
    setIsLogined(state, isLogined) {
      state.isLogined = isLogined;
    },
    setName(state ,name){
      state.name=name;
    },
    setNum(state, num) {
      state.num = num;
    },
    setIdxed(state, idxed) {
      state.idxed = idxed;
    },
    setOption(state, option) {
      state.option = option;
    },
    logout(state){
      state.user=undefined;
      state.isLogined=false;
      state.name=undefined;
    }
  },
  // 异步方法用actions
  // actions不能直接修改全局变量，需要调用commit方法来触发mutation中的方法
  actions: {
    login(context, user, isLogined) {
      context.commit('setUser', user)
      context.commit('setIsLogined', isLogined)
    },
    logout(context) {
      context.commit('setUser', undefined)
      context.commit('setIsLogined', false)
    }
  }

})
export default store
