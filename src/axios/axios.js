import Vue from 'vue'
import axios from 'axios'
import store from '../store/index.js'
import router from '../router/index.js'

axios.defaults.baseURL='http://139.155.243.83:8082/'
Vue.prototype.$ajax=axios

//请求拦截器
//request interceptor
axios.interceptors.request.use(function(config){

    var token=window.sessionStorage.getItem('token');
 if(token){
  config.headers.Authorization=token;
 }

  if(config.method!='get'){
    var sendData =new FormData();
    var data=config.data;
    Object.keys(data).forEach(key =>{
      sendData.append(key,data[key]);
    });
    config.data=sendData
  }
  return config;


},function(error){
  return Promise.reject(error);
})
//响应拦截器
axios.interceptors.response.use(
function(resp){
  return resp;
},
function(error){
  if(error.response){
    switch (error.response.status){
      case 401:
      store.commit('logout');
      router.replace({
        path:'/',
      })
    }
  }
  return Promise.reject(error);
})
export default axios
